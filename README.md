# sudoku_solver

A quick and dirty attempt at solving Sudoku puzzles.

## Usage

Tested with Python 3.7.0, run the included sample puzzle with:

```
python sudoku_solver.py
```

## Credit

Inspiration and naive_solve algorithm come from Computerphile's 
[Python Sudoku Solver Video](https://www.youtube.com/watch?v=G_UYXzGuqvM),
go check them out.

Further inspiration and the first puzzle come from
[Sudoku Sandiway](https://dingo.sbs.arizona.edu/~sandiway/sudoku/examples.html)
at the University of Arizona. Bear Down Arizona!

